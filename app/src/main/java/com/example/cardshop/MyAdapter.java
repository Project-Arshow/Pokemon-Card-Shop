package com.example.cardshop;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Vector;

public class MyAdapter extends BaseAdapter {

    Vector<Bitmap> vector = new Vector<Bitmap>();

    public void add(Bitmap bm) {
        vector.add(bm);
    }

    @Override
    public int getCount() {
        return vector.size();
    }

    @Override
    public Object getItem(int position) {
        return vector.toArray()[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout, viewGroup, false);
        ImageView tv = (ImageView) v.findViewById(R.id.tv);
        tv.setImageBitmap(vector.get(i));
        //tv.setText("Coucou");
        return v;
    }
}
