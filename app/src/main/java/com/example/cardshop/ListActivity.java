package com.example.cardshop;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        MyAdapter adapter = new MyAdapter();
        ListView l = (ListView)findViewById(R.id.list);
        // l.setAdapter(adapter);

        MyDatabase mydb = new MyDatabase(ListActivity.this);

        Bundle extras = getIntent().getExtras();
        String req = new String(extras.getString("request"));

        String url = new String("https://api.pokemontcg.io/v2/cards?q=name:" + req + "&pageSize=5");

        AsyncFlickrJSONDataForList task = new AsyncFlickrJSONDataForList(adapter);
        task.execute(url);
        Log.i("Test", "Adapter calling");
        l.setAdapter(adapter);

        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(ListActivity.this, CardView.class);

                //Passing bitmap to another activity
                Bundle extras = new Bundle();
                Bitmap bm = (Bitmap) adapter.getItem(position);



                Bitmap bitmap = Bitmap.createScaledBitmap(bm, 300, 413, false); //Adapting bitmap size

                String stringBM = Utils.BitMapToString(bitmap);
                mydb.addBitmap(stringBM);

                extras.putParcelable("imagebitmap", bitmap);
                intent.putExtras(extras);
                startActivity(intent);

                Toast.makeText(ListActivity.this, "Added to history", Toast.LENGTH_SHORT).show();
            }
        });

    }
}