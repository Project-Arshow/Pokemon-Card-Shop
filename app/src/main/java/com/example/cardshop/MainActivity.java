package com.example.cardshop;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.BreakIterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyDatabase mydb = new MyDatabase(MainActivity.this);

        Button search = findViewById(R.id.btnSearch);
        EditText request = findViewById(R.id.searchBar);

        ListView history = findViewById(R.id.history);
        MyAdapterByte mainAdapter = new MyAdapterByte();

        mainAdapter.add(Utils.StringToBitMap(mydb.latest()));

        mainAdapter.notifyDataSetChanged();
        history.setAdapter(mainAdapter);

        history.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, CardView2.class);
                Bundle extras = new Bundle();
                Bitmap bm = (Bitmap) mainAdapter.getItem(position);

                Bitmap bitmap = Bitmap.createScaledBitmap(bm, 300, 413, false); //Adapting bitmap size

                extras.putParcelable("imagebitmap2", bitmap);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                intent.putExtra("request", request.getText().toString());
                startActivity(intent);
            }
        });
    }
}