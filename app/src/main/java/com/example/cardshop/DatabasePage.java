package com.example.cardshop;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.time.LocalDate;

public class DatabasePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_page);

        MyDatabase mydb = new MyDatabase(this);

        EditText text = findViewById(R.id.card);
        Button add = findViewById(R.id.add);
        Button read = findViewById(R.id.read);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydb.insertData(text.getText().toString());
                Toast.makeText(DatabasePage.this, "Added successfully !", Toast.LENGTH_SHORT).show();
            }
        });

        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydb.readData();
                Toast.makeText(DatabasePage.this, "Read successfully !", Toast.LENGTH_SHORT).show();
            }
        });
    }
}