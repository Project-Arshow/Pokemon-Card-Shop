package com.example.cardshop;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Vector;

public class MyAdapterByte extends BaseAdapter {
    Vector<Bitmap> vector = new Vector<Bitmap>();

    @Override
    public int getCount() {
        return vector.size();
    }

    @Override
    public Object getItem(int position) {
        return vector.toArray()[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View v =  LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.tv_layout, viewGroup, false);
        ImageView tv2 = v.findViewById(R.id.tv2);
        tv2.setImageBitmap(vector.get(position));
        return v;
    }

    public void set(int i, Bitmap s) {
        vector.set(i, s);
    }

    public void add(Bitmap s) {
        vector.add(s);
    }
}
