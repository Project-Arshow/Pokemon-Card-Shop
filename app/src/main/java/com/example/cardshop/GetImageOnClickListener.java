package com.example.cardshop;

import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class GetImageOnClickListener implements View.OnClickListener {

    private MainActivity mainActivity;

    public GetImageOnClickListener(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onClick(View v){
        Log.i("Test", "");

        String url = new String("https://api.pokemontcg.io/v2/cards?q=name:charizard&pageSize=1");
        AsyncCardJSONData task = new AsyncCardJSONData(mainActivity);
        task.execute(url);

    }
}

